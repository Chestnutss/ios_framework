Pod::Spec.new do |s|
    s.name         = "HRFramework"
    s.version      = "1.0.0"
    s.ios.deployment_target = '9.0'
    s.summary      = "A delightful setting Dev framework."
    s.homepage     = "https://gitee.com/uiview/ios_framework"
    s.license              = { :type => "MIT", :file => "LICENSE" }
    s.author             = { "sun" => "750460196@qq.com" }
    s.source       = { :git => "https://gitee.com/uiview/ios_framework.git", :tag => s.version }
    s.source_files  = "HRFramework/**/*.{h,m}"
    s.requires_arc = true
end

