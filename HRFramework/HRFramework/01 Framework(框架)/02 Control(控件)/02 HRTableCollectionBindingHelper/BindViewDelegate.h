
#import <Foundation/Foundation.h>

@protocol BindViewDelegate <NSObject>
- (void)bindModel:(id)model;
@end
