


#import "HRRequestManager.h"


@interface HRNetworkingManager : HRRequestManager

#pragma mark GET请求
- (void)GET_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure;

#pragma mark POST请求
- (void)POST_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure;

@end
