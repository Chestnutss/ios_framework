
#import "AppDelegate.h"
#import "HRAppdelegateProtocol.h"
#import "UIButton+AddAction.h"
@interface AppDelegate ()<HRAppdelegateProtocol>

@end

@implementation AppDelegate

#pragma mark - ---------- Lifecycle ----------
//程序开始
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self configNetStatusMoniterEnvironment];
    return YES;
}
//程序将要进入后台
- (void)applicationWillResignActive:(UIApplication *)application {
}
//程序已经进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
}
//程序将要进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
}
//程序已经进入前台
- (void)applicationDidBecomeActive:(UIApplication *)application {
}
//程序关闭
- (void)applicationWillTerminate:(UIApplication *)application {
}

#pragma mark - ---------- Private Methods ----------
//配置根视图
- (void)configRootViewController {
    
}
//配置网络监测环境
- (void)configNetStatusMoniterEnvironment {
    [[HRRequest manager] moniterNetStatus:^(HRNetStatus netStatus) {
        if (netStatus == HRNetStatusUnknown || netStatus == HRNetStatusNotReachable) {
            NSLog(@"%@", NET_NOT_WORK);
        }
    }];
}

@end
