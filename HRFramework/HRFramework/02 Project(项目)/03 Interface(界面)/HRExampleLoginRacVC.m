/**
 MVVM + Rac登录示例
 */
#import "HRExampleLoginRacVC.h"
#import "HRExampleLoginViewModel.h"



@interface HRExampleLoginRacVC ()
@property (weak, nonatomic) IBOutlet UITextField *accoutTF;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;
@property (nonatomic,strong)HRExampleLoginViewModel *viewModel;

@end

@implementation HRExampleLoginRacVC

#pragma mark -lazy load
-(HRExampleLoginViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[HRExampleLoginViewModel alloc]init];
    }
    return _viewModel;
}

#pragma mark -life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //点击登录的操作监听
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark -protocal method
-(void)hr_configUI
{
    
}

-(void)hr_getNewData
{
    
}

-(void)hr_bindViewModel
{
    RAC(self.viewModel,accoutStr) = self.accoutTF.rac_textSignal;
    RAC(self.viewModel,pwdStr)    = self.pwdTF.rac_textSignal;
}

@end
