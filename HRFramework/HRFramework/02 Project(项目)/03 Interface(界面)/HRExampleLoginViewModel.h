//
//  HRExampleLoginViewModel.h
//  HRFramework
//
//  Created by lz on 2018/3/15.
//

#import "BaseViewModel.h"

@interface HRExampleLoginViewModel : BaseViewModel

@property(nonatomic,copy)  NSString *accoutStr;
@property(nonatomic,copy)  NSString *pwdStr;

@property(nonatomic,strong,readonly)RACCommand *loginCommand;//登录命令

@end
