//
//  HRExampleLoginViewModel.m
//  HRFramework
//
//  Created by lz on 2018/3/15.
//

#import "HRExampleLoginViewModel.h"

@interface HRExampleLoginViewModel()

@property (nonatomic,strong,readwrite) RACCommand *loginCommand;

@end

@implementation HRExampleLoginViewModel

- (instancetype)init
{
    if (self = [super init]) {
        self.accoutStr = @"";
        self.pwdStr    = @"";
        
        RACSignal * mobileNumSignal = [RACObserve(self, accoutStr) filter:^BOOL(NSString * value) {
            return value.length == 11;
        }];
        
        RACSignal * smsCodeSignal =[RACObserve(self, pwdStr) filter:^BOOL(NSString * value) {
            return value.length == 16;
        }];
        
        @weakify(self);
        [[RACSignal combineLatest:@[mobileNumSignal,smsCodeSignal]] subscribeNext:^(id x) {
            @strongify(self);
            NSLog(@"触发请求");
            //注：这里合并信号后（初始化）直接发送了请求，实际情况下可写一个命令进行请求 单独分离
            [self.loginCommand execute:nil];
        }];
    }
    return self;
}

#pragma mark - get && set
- (RACCommand *)loginCommand{
    
    if (nil == _loginCommand) {
        @weakify(self);
        _loginCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            @strongify(self);
            
            //这里位数以及验证码在控制器那里已经筛选过了,这里只是做个简单的例子,可以在这里对手机正则等进行校验
            //最好的写法:button.rac_command = viewmodel.loginCommand...把位数判断移到这里
            if (self.accoutStr.length != 11) {
                return [RACSignal error:[NSError errorWithDomain:@"" code:10 userInfo:@{@"errorInfo":@"手机号码位数不对"}]];
            }
            if (self.pwdStr.length != 4) {
                return [RACSignal error:[NSError errorWithDomain:@"" code:20 userInfo:@{@"errorInfo":@"验证码位数不对"}]];
            }
            
            return [self loginSignalWithMobileNum:self.accoutStr smsCode:self.pwdStr];
        }];
        
    }
    return _loginCommand;
}

#pragma mark - private

- (RACSignal *)loginSignalWithMobileNum:(NSString *)mobileNo smsCode:(NSString *)authCodeSMS{
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        [[HRRequest manager] POST_URL:@"" params:nil success:^(id result) {
            [subscriber sendNext:result];
            [subscriber sendCompleted];
        } failure:^(NSDictionary *errorInfo) {
            NSError *error;
            [subscriber sendError:error];
        }];
        //完成信号后取消
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}


@end
