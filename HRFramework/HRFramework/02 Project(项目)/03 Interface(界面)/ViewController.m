

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];

    
    NSMutableArray *shopList = [NSMutableArray arrayWithObjects:@{@"id" : @"DDS434313", @"name": @"可乐", @"num" : @"5", @"inventory" : @"400"},
                                @{@"id" : @"OOYU23423", @"name": @"雪碧", @"num" : @"9", @"inventory" : @"300"},
                                @{@"id" : @"PW0973434", @"name": @"咖啡", @"num" : @"1", @"inventory" : @"80"},
                                @{@"id" : @"9JLHG2323", @"name": @"橙汁", @"num" : @"11", @"inventory" : @"200"},
                                @{@"id" : @"JLJLYOYO1", @"name": @"王老吉", @"num" : @"7", @"inventory" : @"150"},
                                @{@"id" : @"DDS434313", @"name": @"可乐", @"num" : @"7", @"inventory" : @"320"}, nil];
    
    
//    NSSet *set = [NSSet setWithArray:shopList];
    
//    NSLog(@"%@",[self repeatData:shopList]);
    
    // 去除数组中model重复
    for (NSInteger i = 0; i < shopList.count; i++) {
        for (NSInteger j = i+1;j < shopList.count; j++) {
            //        AssistantModel *tempModel = self.selectedModelArray[i];
            NSDictionary *dic1 = shopList[i];
            NSDictionary *dic2 = shopList[j];
            //        AssistantModel *model = self.selectedModelArray[j];
//            if ([tempModel.assistantId isEqualToString:model.assistantId]) {
//                [self.selectedModelArray removeObject:model];
//            }
            if ([[dic2 objectForKey:@"id"]isEqualToString:[dic1 objectForKey:@"id"]]) {
                [shopList removeObject:dic2];
            }
        }
    }
    
    NSLog(@"%@",shopList);
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

-(void)hr_configUI
{
    
}


@end

