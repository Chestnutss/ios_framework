

#ifndef Constant_h
#define Constant_h

/*
    //举例
    //是否是第一次进入程序
    static NSString *const USERDEFAULT_FIRST_ENTER = @"USERDEFAULT_FIRST_ENTER"; // ⚠️变量名称全部大写，多个单词用下划线分隔
 */

#pragma mark - ---------- 偏好设置（Userdefault）----------


#pragma mark - ---------- 解归档（Archive）----------


#pragma mark - ---------- 通知（Notification）----------


#pragma mark - ---------- 颜色 (Color) ----------


#pragma mark - ---------- 字体(Font) ----------


#pragma mark - ---------- 其他（Others） ----------

static NSString *const NET_NOT_WORK = @"网络断开连接，请检查网络";

static NSString *const ERROR_MESSAGE = @"网络请求失败";

#endif /* ProjectCommon_h */
