/*
 项目网络请求
 可拓展
 */

#import "HRNetworkingManager.h"

typedef void(^Success)(id data);
typedef void(^Failure)(NSString *errMsg);

@interface HRRequest : HRNetworkingManager

//初始化（单例）
+ (instancetype)manager;

//1.0 GET请求
- (void)GET:(NSString *)path para:(NSDictionary *)para success:(Success)success faiulre:(Failure)failure;

//2.0 POST请求
- (void)POST:(NSString *)path para:(NSDictionary *)para success:(Success)success faiulre:(Failure)failure;

@end
